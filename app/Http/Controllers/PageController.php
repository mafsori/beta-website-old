<?php

namespace App\Http\Controllers;
use App;
use Session;
use Illuminate\Http\Request;


use App\Model\Why;
use App\Model\CanDo;

class PageController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __construct()
    {
        $this->middleware('LanguageSwitcher');
        $locale = session()->get('locale');
        if(empty($locale)){
            session()->put('locale','en');
            
        }
    }

    public function change_language($locale){
        Session::put('locale',$locale);
        App::setLocale($locale);
        return redirect()->back();
    }

    public function index()
    {
        $data_why = Why::where('status',1)->orderBy('urutan')->get();
        $data_can = CanDo::where('status',1)->orderBy('urutan')->get();
        return view('welcome', compact('data_why','data_can'));
    }

    public function comingsoon()
    {
        return view('front.comingsoon');
    }

    public function about_company()
    {
        return view('front.about_company');
    }
    public function about_csr()
    {
        return view('front.about_csr');
    }
    public function careers()
    {
        return view('front.careers');
    }
    public function features()
    {
        return view('front.features');
    }

    public function product_hiapp()
    {
        return view('front.product_hiapp');
    }
    public function product_hiconference()
    {
        return view('front.product_hiconference');
    }
    public function product_hibusiness()
    {
        return view('front.product_hibusiness');
    }
    public function product_hipay()
    {
        return view('front.product_hipay');
    }


    public function security()
    {
        return view('front.security');
    }
    public function faq()
    {
        return view('front.faq');
    }
    public function contact_us()
    {
        return view('front.contact_us');
    }
    public function community()
    {
        return view('front.community');
    }
    public function privacy()
    {
        return view('front.privacy');
    }
    public function download()
    {
        return view('front.download');
    }
    
}
