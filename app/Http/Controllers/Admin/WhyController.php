<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use Image;
use App\Http\Requests;
use Auth;

use App\Model\Why;

class WhyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_all = Why::orderBy('urutan')->get();
        return view('admin.why.index',compact('data_all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.why.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'title_en' => 'required',
        ]);

        if ($valid->fails())
        {
        	
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        }else{ 
        	
            $data = new Why();
            
            $data->title_en = $request->input('title_en');
            $data->title_ind = $request->input('title_ind');
            $data->content_en = $request->input('content_en');
            $data->content_ind = $request->input('content_ind');
            $data->urutan = $request->input('urutan');
            $data->status = $request->input('status');
            

            $file       = $request->file('images');
            if(!empty($file)){
                $fileName   = date('YmdHis').'-'.$file->getClientOriginalName();
                $file->move("upload/why/", $fileName);

                $data->images = $fileName;

            }

            if($data->save()){
                Session::flash('success', 'success save data');
                return redirect('admin/why');
            }
            else{
            	Session::flash('warning', 'error save data');
                $valid = "error save data";
                return redirect()->back()
                    ->withErrors($valid)
                    ->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Why::where('id',$id)->first();
        if(empty($data)){
            Session::flash('warning', 'data not found');
            return redirect()->back();
        }
        return view('admin.why.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $valid = Validator::make($request->all(), [
            'title_en' => 'required',
        ]);

        if ($valid->fails())
        {
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        }else{ 
        	
	        $data = Why::where('id',$id)->first();
	        if(empty($data)){
	            Session::flash('warning', 'data not found');
	            return redirect()->back();
	        }
			
            $data->title_en = $request->input('title_en');
            $data->title_ind = $request->input('title_ind');
            $data->content_en = $request->input('content_en');
            $data->content_ind = $request->input('content_ind');
            $data->urutan = $request->input('urutan');
            $data->status = $request->input('status');
            

            $file       = $request->file('images');
            if(!empty($file)){
                $fileName   = date('YmdHis').'-'.$file->getClientOriginalName();
                $file->move("upload/why/", $fileName);

                $file_delete = public_path().'/upload/why/'.$data->images;
                \File::delete($file_delete);

                $data->images = $fileName;

            }

            if($data->update()){
                Session::flash('success', 'success save data');
                return redirect('admin/why');
            }
            else{
            	Session::flash('warning', 'error save data');
                $valid = "error save data";
                return redirect()->back()
                    ->withErrors($valid)
                    ->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Why::where(['id' => $id])->first();

        if (empty($data)) {
            Session::flash('warning', 'Delete Error');
            return redirect()->back();
        }
        $file_delete = public_path().'/upload/why/'.$data->images;
        \File::delete($file_delete);
        if ($data->delete()) {
            Session::flash('success', 'Delete Success');
        }else{
            Session::flash('warning', 'Delete Error, relasi tabel');
        }
            
        return redirect()->back();
        
    }
}
