<?php



namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Validator;
use Session;
use Image;
use App\Http\Requests;
use Auth;

use App\Model\Website;

class WebsiteController extends Controller
{
    public function index()
    {
        $data = Website::latest()->first();
        return view('admin.website',compact('data'));
    }
    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'url' => 'required',
            'name' => 'required',
            'caption' => 'required',   
        ]);

        if ($valid->fails())
        {
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        }else{ 
            $data = Website::latest()->first();
            $data->url = $request->input('url');
            $data->name = $request->input('name');
            $data->caption = $request->input('caption');
            $data->facebook = $request->input('facebook');
            $data->twitter = $request->input('twitter');
            $data->instagram = $request->input('instagram');
            $data->linkedin = $request->input('linkedin');
            $data->seo_keyword = $request->input('seo_keyword');
            $data->seo_tag = $request->input('seo_tag');
            $data->seo_title = $request->input('seo_title');
            $data->seo_desc = $request->input('seo_desc');

            $file       = $request->file('logo');
            if(!empty($file)){
                $fileName   = date('YmdHis').'logo.'.$file->getClientOriginalExtension();
                // Image::make($file)->save("upload/website/".$fileName);
                $file->move("upload/website/", $fileName);

                if($data->logo != 'logo.png'){
                    $file_delete = public_path().'/upload/website/'.$data->logo;
                    \File::delete($file_delete);
                }
                $data->logo = $fileName;

            }
            $file_icon       = $request->file('icon');
            if(!empty($file_icon)){
                $file_iconName   = date('YmdHis').'icon.'.$file_icon->getClientOriginalExtension();
                // Image::make($file_icon)->save("upload/website/".$file_iconName);
                $file_icon->move("upload/website/", $file_iconName);

                if($data->icon != 'icon.png'){
                    $file_icon_delete = public_path().'/upload/website/'.$data->icon;
                    \File::delete($file_icon_delete);
                }
                $data->icon = $file_iconName;

            }
            
            if($data->update()){
                Session::flash('success', 'sukses simpan data');
                return redirect()->back();
            }
            else{
                $valid = "gagal simpan data";
                return redirect()->back()
                    ->withErrors($valid)
                    ->withInput();
            }
        }
    }
}
