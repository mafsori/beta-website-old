<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Session;
use Image;
use App\Http\Requests;
use Auth;

use App\User;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_admin = User::all();
        return view('admin/user',compact('user_admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name'  => 'required',
            'email' => 'required|email|max:255|unique:users',
            'password'  => 'required',
        ]);

        if ($valid->fails())
        {
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        }else{ 
            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $file_avatar       = $request->file('avatar');
            if(!empty($file_avatar)){
                $file_avatarName   = date('YmdHis').'avatar.'.$file_avatar->getClientOriginalExtension();
                Image::make($file_avatar)->save("upload/user/".$file_avatarName);

                
                $user->avatar = $file_avatarName;

            }
            if($user->save()){
                Session::flash('success', 'user level berhasil di simpan');
                return redirect('admin/users');
            }
            else{
                $valid = "harap isian jangan kosong";
                return redirect()->back()
                    ->withErrors($valid)
                    ->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name'  => 'required',
        ]);

        if ($valid->fails())
        {
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        }else{ 
            $id = $request->input('id');
            $admin =  User::find($id);
            $admin->name = $request->input('name');
            
            if($request->input('password') != ""){
                $admin->password = bcrypt($request->input('password'));
            }
            $file_avatar       = $request->file('avatar');
            if(!empty($file_avatar)){
                $file_avatarName   = date('YmdHis').'avatar.'.$file_avatar->getClientOriginalExtension();
                Image::make($file_avatar)->save("upload/user/".$file_avatarName);

                if($admin->avatar != 'user.jpg'){
                    $file_avatar_delete = public_path().'/upload/user/'.$admin->avatar;
                    \File::delete($file_avatar_delete);
                }
                $admin->avatar = $file_avatarName;

            }
            if($admin->update()){
                Session::flash('success', 'user level berhasil di simpan');
                return redirect('admin/users');
            }
            else{
                $valid = "harap isian jangan kosong";
                return redirect()->back()
                    ->withErrors($valid)
                    ->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = User::where(['id' => $id])->first();

        if (empty($model)) {
            Session::flash('warning', 'Delete Error');
            return redirect()->back();
        }

        if ($model->delete()) {
            Session::flash('success', 'Delete Success');
        }
        return redirect()->back();
    }

    public function profile()
    {
        return view('admin/profile');
    }

    public function profile_update(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name'  => 'required',
        ]);

        if ($valid->fails())
        {
            return redirect()->back()
                ->withErrors($valid)
                ->withInput();
        }else{ 
            $id = Auth::user()->id;
            $admin =  User::find($id);
            $admin->name = $request->input('name');
            
            if($request->input('password') != ""){
                $admin->password = bcrypt($request->input('password'));
            }
            $file_avatar       = $request->file('avatar');
            if(!empty($file_avatar)){
                $file_avatarName   = date('YmdHis').'avatar.'.$file_avatar->getClientOriginalExtension();
                Image::make($file_avatar)->save("upload/user/".$file_avatarName);

                if($admin->avatar != 'user.jpg'){
                    $file_avatar_delete = public_path().'/upload/user/'.$admin->avatar;
                    \File::delete($file_avatar_delete);
                }
                $admin->avatar = $file_avatarName;

            }
            if($admin->update()){
                Session::flash('success', 'user level berhasil di simpan');
                return redirect()->back();
            }
            else{
                $valid = "harap isian jangan kosong";
                return redirect()->back()
                    ->withErrors($valid)
                    ->withInput();
            }
        }
    }



}
