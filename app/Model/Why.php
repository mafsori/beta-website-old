<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Why extends Model
{
    protected $table = 'why';
    protected $primaryKey = 'id';
    protected $fillable = [
		'title_en',
		'title_ind',
		'content_en',
		'content_ind',
		'images',
		'urutan',
		'coming_soon',
		'status',

    ];

    public function url_images()
    {
        $urlnya = "";
        if($this->images != null){
            $urlnya = asset('upload/why').'/'.$this->images;
        }
        return  $urlnya;
    }

    public function status_text(){
        $status = HelperData::list_status();
        return isset($status[$this->status])?$status[$this->status]:'';
    }
}
