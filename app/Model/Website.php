<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    //
    protected $table = 'websites';
    protected $primaryKey = 'id';
    protected $fillable = [
		'url',
		'name',
		'caption',
		'icon',
		'logo',
		'facebook',
		'twitter',
		'linkedin',
		'instagram',
		'seo_keyword',
		'seo_tag',
		'seo_title',
		'seo_desc',

    ];

    public function url_logo()
    {
        $urlnya = "";
        if($this->logo != null){
            $urlnya = asset('upload/website').'/'.$this->logo;
        }
        return  $urlnya;
    }

    public function url_icon()
    {
        $urlnya = "";
        if($this->icon != null){
            $urlnya = asset('upload/website').'/'.$this->icon;
        }
        return  $urlnya;
    }
}
