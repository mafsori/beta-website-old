<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HelperData extends Model {

    //
    public static function list_status() {
        return array(
            1 => 'Publish',
            2 => 'Draf',
        );
    }
}
