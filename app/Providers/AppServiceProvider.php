<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use SEOMeta;
use OpenGraph;
use Twitter;
## or
use SEO;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $data = \App\Model\Website::latest()->first();

        SEOMeta::setTitle($data->name .' - '.$data->caption);
        SEOMeta::setDescription($data->seo_desc);
        SEOMeta::addMeta('article:published_time', '2020-01-31T20:30:11-02:00', 'property');
        SEOMeta::addMeta('article:section', '', 'property');
        SEOMeta::addKeyword($data->seo_keyword);

        OpenGraph::setDescription($data->seo_desc);
        OpenGraph::setTitle($data->seo_title .' - '.$data->caption);
        OpenGraph::setUrl($data->url);
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'pt-br');
        OpenGraph::addProperty('locale:alternate', ['pt-pt', 'en-us']);

        OpenGraph::addImage($data->url_logo());
        OpenGraph::addImage($data->url_logo());
        OpenGraph::addImage(['url' => $data->url_logo(), 'size' => 300]);
        OpenGraph::addImage($data->url_logo(), ['height' => 300, 'width' => 300]);

        // Namespace URI: http://ogp.me/ns/article#
        // article
        OpenGraph::setTitle($data->seo_title .' - '.$data->caption)
            ->setDescription($data->seo_desc)
            ->setType('article')
            ->setArticle([
                'published_time' => $data->created_at,
                'modified_time' => $data->updated_at,
                'author' => $data->name,
                'section' => 'string',
                'tag' => $data->seo_tag
            ]);

        view()->share('website', $data);

        Schema::defaultStringLength(191);
    }
}
