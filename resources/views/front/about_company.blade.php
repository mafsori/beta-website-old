@extends('layouts.front')

@section('content')
<section id="about_video">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-md-6">
				<div class="content_video">
					<img src="{{ asset('assets/images/bg_about_video.png') }}" class="img-fluid img_video">
					<div class="overlay d-flex align-items-center justify-content-center">
						<img src="{{ asset('assets/images/icon_play.svg') }}">
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="about_video_right">
					<h3>About HiApp</h3>
					<div class="content_video">
						<p>HiApp is a multi-purpose messaging, social media mobile payment app system, which avalaible on phones all over the world, developed by a tech company named PT. Hello Kreasi Indonesia. </p>
						<p>Our aim is ‘Connecting Easily’ people anywhere through HiApp. We continuously innovate to make people's lives easier and bring Indonesia's tech scene into the next level. The name Hi stands for ‘Hello Indonesia’ and yes, it is built by Indonesian eager young  dreamers to build our future here.</p>
						<p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="about_visi_misi">
	<div class="container">
		<div class="row">
			<div class="col-md-10 offset-md-1">
				<div class="row">
					<div class="col-md-6">
						<div class="visi">
							<div class="text-center">
								<img src="{{ asset('assets/images/about_task.svg') }}" class="img-fluid">
							</div>
							
							<h3>Our Mission</h3>
							<div class="content">
								Provide a secure ecosystem that connects people to everything in one platform
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="misi">
							<div class="text-center">
								<img src="{{ asset('assets/images/about_forming.png') }}" class="img-fluid">
							</div>
							<h3>Our Vision</h3>
							<div class="content">
								Become a prominent all-in-one messenger (super app) that facilitates the pace of the digital economy in Indonesia
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="about_our_value" class="text-center">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h3 class="section_title">Our Values in Action</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 ">
				<div class="item_value value_1">
					<img src="{{ asset('assets/images/icons8-Integrity.svg') }}" class="img-fluid">
					<h3>Integrity</h3>
					<div class="content">
						We seek truthfulness, honesty, ethical and fairness with our users, we behave with integrity.
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="item_value value_1">
					<img src="{{ asset('assets/images/icons8-Innovation.svg') }}" class="img-fluid">
					<h3>Innovation</h3>
					<div class="content">
						We innovate and embrace new things in technology to contribute for a brighter Indonesia. We look out from different perspectives other ideas and opinions. 
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="item_value value_1">
					<img src="{{ asset('assets/images/icons8-Responsbility.svg') }}" class="img-fluid">
					<h3>Responsbility</h3>
					<div class="content">
						Users are the heart of everything we do. We understand that each of us has an impact on users relations through the work we do, which is why we always act with integrity and to the highest professional standards
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="item_value value_1">
					<img src="{{ asset('assets/images/icons8-Diversity.svg') }}" class="img-fluid">
					<h3>Diversity</h3>
					<div class="content">
						We respect differences whomever they are and always courteous
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="item_value value_1">
					<img src="{{ asset('assets/images/icons8-Corporate.svg') }}" class="img-fluid">
					<h3>Corporate Responsibility</h3>
					<div class="content">
						Technology is a powerful force that can create benefits and opportunities for everyone. To achieve a better life for everyone is our commitment.
					</div>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="item_value value_1">
					<img src="{{ asset('assets/images/icons8-We.svg') }}" class="img-fluid">
					<h3>We Are One Hi</h3>
					<div class="content">
						Great things happen when we work together as ONE HI (Hello Indonesia) society.
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<section id="about_message">
	<div class="container">
		<div class="row d-flex align-items-center ">
			<div class="col-md-6 item_message_left">
				<img src="{{ asset('assets/images/about_person.png') }}" class="img-fluid">
			</div>
			<div class="col-md-6 item_message_right">
				<h5>Message From CEO</h5>
				<h3>Leading Messenger App for Business Solution.</h3>
				<div class="content">
					<p>Lorem ipsum dolor sit amet, hendrerit omittantur mel, es vidit animal iracundia. Ius te altera essent incorrupte. Id alien accu consetetur eam, nibh aliquam iracundia.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					<div class="name">Samsan Novia, <span>CEO of HiApp</span></div>
					<img src="{{ asset('assets/images/ttd.png') }}" alt="">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection