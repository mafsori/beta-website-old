@extends('layouts.front')

@section('content')
<section id="banner_careers">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 ">
				<h3>Tomorrow won't exist without today</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
				</div>
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<form class="search_job">
							<div class="input-group mb-3">
								
								<input type="text" class="form-control" placeholder="Search your desired job" aria-label="Search your desired job" aria-describedby="basic-addon1">
								<div class="input-group-append">
								    <button class="btn" type="button" id="button-addon2"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</form>
					</div>
				</div>
						
			</div>
		</div>
	</div>
</section>
<section id="variety">
	<div class="container">
		<h3 class="section_title">Our Variety Programs</h3>
		<div class="row">
			<div class="col-md-10 offset-md-1">
				<div class="row">
					<div class="col-md-6">
						<div class="variety_item">
							<h4>Students and recent graduates </h4>
							<div class="content">
								We offer young generations the opportunities to empower themselves and be part of something genuinely special that impact the society. 
							</div>
							<a href="" class="more">See Available Position</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="variety_item">
							<h4>Experienced professionals</h4>
							<div class="content">
								Passionate about technology and want to make a difference?Come and work with us and you'll have opportunities to take your career to the next level.
							</div>
							<a href="" class="more">See Available Position <i class="fa fa-chevron-right ml-3"></i></a>
						</div>
					</div>
				</div>
			</div>
					
		</div>
	</div>
</section>
<section id="careers_life">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-md-6 item_careers_life">
				<div class="content_video">
					<img src="{{ asset('assets/images/bg_video_karir.png') }}" class="img-fluid img_video">
					<div class="overlay d-flex align-items-center justify-content-center">
						<img src="{{ asset('assets/images/icon_play.svg') }}">
					</div>
				</div>
			</div>
			<div class="col-md-6 item_careers_life">
				<h4>Life at HiApp</h4>
				<div class="content">
					HiApp is proud to be an Equal Employment Opportunity and Affirmative Action employer. We do not discriminate based upon race, religion, color, national origin, sex (including pregnancy, childbirth, or related medical conditions), sexual orientation, gender identity, gender expression, age, status as a protected veteran, status as an individual with a disability, genetic information, or other applicable legally protected characteristics.
				</div>
			</div>
		</div>
	</div>
</section>
<section id="careers_position">
	<div class="container">
		<div class="row top_career d-flex align-items-center">
			<div class="col-md-6">
				<div class="subtitle">See Available Position</div>
				<h3>By Department</h3>
			</div>
			<div class="col-md-6">
				<ul class="nav justify-content-end nav_tab">
					<li class="nav-item">
						<a class="nav-link active" href="#">All</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Product</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Developer</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Marketing</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Finance</a>
					</li>
					
				</ul>
			</div>
		</div>
		<div class="content_career">
			@for($i=1;$i<10;$i++)
			<div class="row item_careers d-flex align-items-center py-3">
				<div class="col-md-6">
					<div class="judul">Backend Developer</div>
					<div class="d-flex align-items-center">
						<div class="jam mr-3">Developer - <span>Fulltime</span></div>
						<div class="lokasi">
							<i class="fa fa-map-marker"></i> Yogyakarta
						</div>
					</div>
						
				</div>
				<div class="col-md-6 career-right">
					<a href="" class="btn btn_career">See Details</a>
				</div>
			</div>
			@endfor
		</div>		

	</div>	
</section>
@endsection