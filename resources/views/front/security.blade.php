@extends('layouts.front')

@section('content')
<section id="banner_security">
	<img src="{{ asset('assets/images/bg_banner_security.svg') }}" class="banner_img">
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<h3>Why HiApp is Secure Messenger</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
				</div>
				<div class="text-center">
					<a href="#content_security" class="btn btn_main">See Securitys</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="content_security">
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3 section_page ">
				<h3>Why HiApp is Secure?</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
				</div>
			</div>
		</div>
				
	</div>
	<div class="container list_security">
		<div class="row">
			@for($i=1;$i<7;$i++)
			<div class="col-12 col-sm-6 col-md-4">
				<div class="item_security text-center">
					<img src="{{ asset('assets/images/icon_powerful.png') }}" class="img-fluid">
					<h3>Security {{ $i }}</h3>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam
					</div>
				</div>
			</div>
			@endfor
		</div>
	</div>
</section>
@endsection