@extends('layouts.front')

@section('content')
<section id="banner_csr">
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3 banner_csr ">
				<h3>Share Kindness to Everyone</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
				</div>
				<a href="" class="btn btn_main">Browse Program</a>
			</div>
		</div>
	</div>
</section>
<section id="csr_about">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="content_video">
					<img src="{{ asset('assets/images/bg_video_karir.png') }}" class="img-fluid img_video">
					<div class="overlay d-flex align-items-center justify-content-center">
						<img src="{{ asset('assets/images/icon_play.svg') }}">
					</div>
				</div>
			</div>

			<div class="col-md-6 about_right">
				<h4>About HiApp CSR</h4>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor rhoncus dolor purus non enim praesent elementum facilisis leo, vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in ornare quam viverra orci sagittis eu volutpat odio facilisis mauris sit amet massa vitae tortor condimentum lacinia quis vel eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue
				</div>
			</div>
		</div>
	</div>
</section>
<section id="our_csr">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2 section_page text-center ">
				<h3>Our CSR Programs</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
				</div>
			</div>
		</div>
				
	</div>
	<div class="container program">
		<div class="row">
			
			<div class="col-md-4">
				<div class="item_programs text-center">
					<img src="{{ asset('assets/images/icons8-Education.svg') }}" class="img-fluid">
					<h3>Education</h3>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="item_programs text-center">
					<img src="{{ asset('assets/images/icons8-Human Resource.svg') }}" class="img-fluid">
					<h3>Human Resource</h3>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="item_programs text-center">
					<img src="{{ asset('assets/images/icons8-Nature.svg') }}" class="img-fluid">
					<h3>Nature</h3>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="csr_testimoni">
	<div class="container">
		<div class="row">
			<div class="col-md-6 testimoni_left">
				<h3>Lets hear what they said about the programs</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut 
				</div>
			</div>
			<div class="col-md-6 testimoni_right">
				<div class="slide_testimoni slider">
					@for($i=1;$i<4;$i++)
					<div class="item_testimony">
						<div class="media">
							<img src="{{ asset('assets/images/user/'.$i.'.jpg') }}" class="align-self-center mr-3" alt="...">
							<div class="media-body">
								<h5 class="mt-0">Hamdan Mukoffan</h5>
								<p>SD 05 Jakarta Teacher</p>
							</div>
						</div>
						<div class="content">
							Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididunt ut labore et dolore magna ad minim veniam, quis nostrud exercitation ullamco 
						</div>
					</div>
					@endfor
				</div>
			</div>
		</div>
	</div>
</section>
<section id="csr_activity">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3 class="section_title">CSR Activity</h3>
			</div>
			@for($i=1;$i<4;$i++)
				<div class="col-md-4">
					<div class="item_actifity">
						<div class="bg_img" style="background-image: url('{{ asset('assets/images/csr_'.$i.'.png') }}')"></div>
						<h4>
							<a href="" class="link_text">Lorem ipsum dolor sit amet, consectet </a>
						</h4>
						<div class="content">
							This response is important for our ability to learn from mistakes, but it alsogives rise to self-criticism, because it is 
						</div>
						<a href="" class="link_text">Read More <i class="fa fa-"></i></a>
					</div>
				</div>
			@endfor
		</div>
	</div>
</section>
@endsection