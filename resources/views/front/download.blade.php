@extends('layouts.front')

@section('content')
{{-- @include('layouts.under') --}}
<section id="banner_download" class="text-center py-5">
	<div class="container">
		<div class="col-md-8 offset-md-2">
			<h3> Try HiApp for Free, Best Messenger</h3>
			<div class="content">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
			</div>
			<ul class="nav justify-content-center download_store">
	            <li class="nav-item">
	                <a class="nav-link" href="#">
	                    <img src="{{ asset('assets/images/download/google_play_light.svg') }}">
	                </a>
	            </li>
	            <li class="nav-item">
	                <a class="nav-link" href="#">
	                    <img src="{{ asset('assets/images/download/app_store_light.svg') }}">
	                </a>
	            </li>
	        </ul>
		</div>
			
	</div>
</section>
<div id="bg_download">
	<div class="row d-flex align-items-end">
		<div class="col-md-6 p-0">
			<img src="{{ asset('assets/images/bg_download_left.png') }}" class="w-100">
		</div>
		<div class="col-md-6 p-0">
			<img src="{{ asset('assets/images/bg_download_right.png') }}" class="w-100">
		</div>
	</div>
</div>
@endsection