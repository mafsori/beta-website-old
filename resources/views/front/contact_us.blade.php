@extends('layouts.front')

@section('content')
<section id="banner_contact_us">
	<img src="{{ asset('assets/images/bg_banner_contact.svg') }}" class="banner_img">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-md-5 offset-md-1">
				<div class="banner_contact_us_left text-center">
					<img src="{{ asset('assets/images/support/icon_contact.svg') }}" class="img-fluid">
				</div>
			</div>
			<div class="col-md-6">
				<div class="banner_contact_us_right">
					<h3>Hello!</h3>
					<div class="content">
						<p>We are here to assist and help any of your enquiries about HiApp</p>
						
					</div>
					<a href="">Need Some Help <i class="fa fa-long-arrow-right ml-3"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="contact_support">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="item_support">
					<img src="{{ asset('assets/images/support/img_support.svg') }}" alt="" class="img-fluid">
					<h5>HiApp Support</h5>
					<div class="content">
						For all questions related to HiApp, contact our support team at 
						<a href="mailto:hello@hiapp.id">hello@hiapp.id</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="item_support">
					<img src="{{ asset('assets/images/support/img_general.svg') }}" alt="" class="img-fluid">
					<h5>HiApp General Business Questions</h5>
					<div class="content">
						For all other business questions, contact our business team at
						<a href="mailto:marketing@hiapp.id">marketing@hiapp.id</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="item_support">
					<img src="{{ asset('assets/images/support/img_community.svg') }}" alt="" class="img-fluid">
					<h5>HiApp Community</h5>
					<div class="content">
						Help us to develop a better product, give us your review at 
						<a href="mailto:community@hiapp.id">community@hiapp.id</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="contact_map">
	<div class="container">
		<h3 class="section_title text-center">Our Offices</h3>
		<div class="row">
			<div class="col-md-6">
				<div class="item_alamat">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.262484516449!2d106.82333131497278!3d-6.229084662738303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e4ec0e59a5%3A0x310fdeb3ffe05c77!2sThe%20East!5e0!3m2!1sen!2sid!4v1578573597825!5m2!1sen!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					<h3>Jakarta</h3>
					<div class="alamat">
						The East Tower. 33rd Floor Unit 1-5 Jalan Dr. Ide Anak Agung Gde Agung Kav E3.2 No.1. Kuningan Timur Setiabudi Jakarta Selatan 12950

					</div>
				</div>
					
			</div>
			<div class="col-md-6">
				<div class="item_alamat">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.3762081926916!2d110.37048851467125!3d-7.749861794414292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a58feacdd5bcb%3A0x151ef3d421dab1e4!2sJl.%20Lempongsari%20Raya%20No.3f%2C%20Sumberan%2C%20Sariharjo%2C%20Kec.%20Ngaglik%2C%20Kabupaten%20Sleman%2C%20Daerah%20Istimewa%20Yogyakarta%2055581!5e0!3m2!1sid!2sid!4v1578495168097!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
					<h3>Yogyakarta</h3>
					<div class="alamat">
						Jl. Lempongsari Raya No.3f Sumberan, Sariharjo Kec. Ngaglik, Kabupaten Sleman Daerah Istimewa Yogyakarta 55581

					</div>
				</div>
					
			</div>
		</div>
	</div>
</section>
@endsection