@extends('layouts.front')

@section('content')
<section id="banner_featured">
	<img src="{{ asset('assets/images/bg_banner_featured.svg') }}" class="banner_img">
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3">
				<h3>Why HiApp is Secure Messenger</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
				</div>
				<div class="text-center">
					<a href="#content_featured" class="btn btn_main">See Feature</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="content_featured">
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3 section_page ">
				<h3>Our Feature?</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
				</div>
			</div>
		</div>
				
	</div>
	<div class="container list_featured">
		<div class="row">
			@for($i=1;$i<7;$i++)
			<div class="col-12 col-sm-6 col-md-4">
				<div class="item_feature text-center">
					<img src="{{ asset('assets/images/icon_powerful.png') }}" class="img-fluid">
					<h3>Feature {{ $i }}</h3>
					<div class="content">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam
					</div>
				</div>
			</div>
			@endfor
		</div>
	</div>
</section>
@endsection