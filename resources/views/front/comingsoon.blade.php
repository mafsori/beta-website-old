<!DOCTYPE html>
<html>
    <head>
        <title>Hi App - Fast And Secure Messenger</title>
        <link rel="shortcut icon" href="{{ asset('assets_comingsoon/images/logo.png') }}">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <link href="{{ asset('assets_comingsoon/css/hiapp.css?v=1') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="overlay"></div>
        <div class=" container text-center">
            <div class="main text-center mt-5">

                <img src="{{ asset('assets_comingsoon/images/logo.png') }}" class="logo">
                <h1 id="title" class="mt-4">Coming Soon</h1>
                <p class="mt-3 mb-3">
                    Connecting is in your Hands<br>
                    Be the First to Know When HIapp Launches! <br>
                    Join the list
                </p>
                <div id="countdown" class="ClassyCountdownDemo mt-5 pt-5 d-none"></div>
                
                <div class="subscribe mt-4">
                    <div class="contact-form">
                        <form action="{{ url('subcribe') }}" method="post">
                            {{ csrf_field() }}
                            <input type="email" name="email" placeholder="Enter You E-mail " required="">
                            <input type="submit" value="Notify Me">
                        </form>

                    </div>
                </div>
                <ul class="sosmed mt-3 ">
                    <li><a href="" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
                
        </div>
        <script src="{{ asset('assets_comingsoon/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets_comingsoon/js/jquery.knob.js') }}"></script>
        <script src="{{ asset('assets_comingsoon/js/jquery.throttle.js') }}"></script>
        <script src="{{ asset('assets_comingsoon/js/jquery.classycountdown.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets_comingsoon/vendor/sweetalert2/sweetalert2.min.js') }}"></script>
        <script>
        $(document).ready(function() {
            @if(Session::has('success')) 
                swal({
                    title: 'Success',
                    text: '{{ Session::get('success') }}',
                    type: 'success',
                    timer: 2000
                });
            @endif

            @if(Session::has('warning')) 
                swal({
                    title: 'Error',
                    text: '{{ Session::get('warning') }}',
                    type: 'warning',
                    timer: 2000
                });
            @endif

            var indoTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Bangkok"});
            indoTime = new Date(indoTime);
            $('#countdown').ClassyCountdown({
                now: indoTime / 1000,
                end: Date.UTC(2020, 1, 1, 0, 0, 0) / 1000,
                labels: true,
                style: {
                    element: "",
                    textResponsive: .5,
                    days: {
                        gauge: {
                            thickness: .10,
                            bgColor: "rgba(255,255,255)",
                            fgColor: "#57b60f",
                            lineCap: 'round'
                        },
                        textCSS: 'font-weight:300; color:#fff;'
                    },
                    hours: {
                        gauge: {
                            thickness: .10,
                            bgColor: "rgba(255,255,255)",
                            fgColor: "#57b60f",
                            lineCap: 'round'
                        },
                        textCSS: ' font-weight:300; color:#fff;'
                    },
                    minutes: {
                        gauge: {
                            thickness: .10,
                            bgColor: "rgba(255,255,255)",
                            fgColor: "#57b60f",
                            lineCap: 'round'
                        },
                        textCSS: ' font-weight:300; color:#fff;'
                    },
                    seconds: {
                        gauge: {
                            thickness: .10,
                            bgColor: "rgba(255,255,255)",
                            fgColor: "#57b60f",
                            lineCap: 'round'
                        },
                        textCSS: ' font-weight:300; color:#fff;'
                    }
                },
                onEndCallback: function() {
                    console.log("Time out!");
                }
            });
        });
        </script>
    <script type="text/javascript">
        document.write("</bod"+"y>"+"</ht"+"ml>");
    </script>