@extends('layouts.front')

@section('content')
<section id="banner_faq">
	<img src="{{ asset('assets/images/bg_banner_faq.svg') }}" class="banner_img">
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-3 ">
				<h3>Need some help from us?</h3>
				<div class="content">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
				</div>
			</div>
			<div class="col-md-8 offset-md-2">
				<form class="search_faq">
					<div class="input-group mb-3">
						
						<input type="text" class="form-control" placeholder="Search your issue" aria-label="Search your issue" aria-describedby="basic-addon1">
						<div class="input-group-append">
						    <button class="btn" type="button" id="button-addon2"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<section id="faq_category">
	<div class="container">
		<h3 class="section_title">Browse Category</h3>
		<div class="row">
			<div class="col-12 col-sm-6 col-md-3">
				<div class="item_category">
					<img src="{{ asset('assets/images/icons8-HiChat.svg') }}" class="img-fluid">
					<h6>HiChat</h6>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-3">
				<div class="item_category">
					<img src="{{ asset('assets/images/icons8-HiConference.svg') }}" class="img-fluid">
					<h6>HiConference</h6>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-3">
				<div class="item_category">
					<img src="{{ asset('assets/images/icons8-HiBusiness.svg') }}" class="img-fluid">
					<h6>HiBusiness</h6>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-md-3">
				<div class="item_category">
					<img src="{{ asset('assets/images/icons8-HiPay.svg') }}" class="img-fluid">
					<h6>HiPay</h6>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="faq_list">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<div class="row">
					<div class="col-md-10 offset-md-1 section_faq">
						<h3>Frequently Asked Question</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 offset-md-1">
				<div class="accordion" id="accordionFaq">
					<div class="card mb-3 card_faq">
						<div class="card-body">
							<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#faq_0" aria-expanded="true" aria-controls="faq_0">
							How can i set my profile picture?
							</button>
							<div id="faq_0" class="collapse show" aria-labelledby="" data-parent="#accordionFaq">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								
							</div>
						</div>
							
					</div>
					<div class="card mb-3 card_faq">
						<div class="card-body">
							<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#faq_1" aria-expanded="true" aria-controls="faq_1">
							Can i Have Multiple Account?
							</button>
							<div id="faq_1" class="collapse" aria-labelledby="" data-parent="#accordionFaq">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								
							</div>
						</div>
							
					</div>
					<div class="card mb-3 card_faq">
						<div class="card-body">
							<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#faq_2" aria-expanded="true" aria-controls="faq_2">
							How to verify my HiChat Account?
							</button>
							<div id="faq_2" class="collapse" aria-labelledby="" data-parent="#accordionFaq">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								
							</div>
						</div>
							
					</div>
					<div class="card mb-3 card_faq">
						<div class="card-body">
							<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#faq_3" aria-expanded="true" aria-controls="faq_3">
							Can i Have Multiple Account?
							</button>
							<div id="faq_3" class="collapse" aria-labelledby="" data-parent="#accordionFaq">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								
							</div>
						</div>
					</div>
					<div class="card mb-3 card_faq">
						<div class="card-body">
							<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#faq_3" aria-expanded="true" aria-controls="faq_3">
							How to verify my HiChat Account?
							</button>
							<div id="faq_3" class="collapse" aria-labelledby="" data-parent="#accordionFaq">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>
@endsection