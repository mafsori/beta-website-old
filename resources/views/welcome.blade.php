@extends('layouts.front')

@section('content')
<style> 

h1{ 
color: #12B505; 

font-weight: 100; 
font-size: 40px; 
margin: 40px 0px 20px; 
} 
#clockdiv{ 
    font-family: 'MuseoSansRounded-100_0', sans-serif;
    color: #fff; 
    display: inline-block; 
    font-weight: 100; 
    text-align: center; 
    font-size: 30px; 
} 
#clockdiv > div{ 
    padding: 10px; 
    border-radius: 3px; 
    background: #12B505; 
    display: inline-block; 
} 
#clockdiv div > span{ 
    padding: 15px; 
    border-radius: 3px; 
    background: #00816A; 
    display: inline-block; 
} 
smalltext{ 
    padding-top: 5px; 
    font-size: 16px; 
} 
</style> 
<script> 

var deadline = new Date("oct 20, 2020 00:00:01").getTime(); 

var x = setInterval(function() { 

var now = new Date().getTime(); 
var t = deadline - now; 
var days = Math.floor(t / (1000 * 60 * 60 * 24)); 
var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60)); 
var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)); 
var seconds = Math.floor((t % (1000 * 60)) / 1000); 
document.getElementById("day").innerHTML =days ; 
document.getElementById("hour").innerHTML =hours; 
document.getElementById("minute").innerHTML = minutes; 
document.getElementById("second").innerHTML =seconds; 
if (t < 0) { 
        clearInterval(x); 
        document.getElementById("demo").innerHTML = "TIME UP"; 
        document.getElementById("day").innerHTML ='0'; 
        document.getElementById("hour").innerHTML ='0'; 
        document.getElementById("minute").innerHTML ='0' ; 
        document.getElementById("second").innerHTML = '0'; } 
}, 1000); 
</script> 
<div id="banner_hero" class="jumbotron jumbotron-fluid" >
    <video id="video" playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="{{ asset('assets/video/intro.webm') }}" type="video/webm">
    </video>
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col-md-5 text-center">
                <img src="{{ asset('assets/images/hp_home_banne.png') }}" class="img-fluid">
            </div>
            <div class="col-md-7 text-center content_right">
                <div id="clockdiv"> 
<div> 
    <span class="days" id="day"></span> 
    <div class="smalltext">Days</div> 
</div> 
<div> 
    <span class="hours" id="hour"></span> 
    <div class="smalltext">Hours</div> 
</div> 
<div> 
    <span class="minutes" id="minute"></span> 
    <div class="smalltext">Minutes</div> 
</div> 
<div> 
    <span class="seconds" id="second"></span> 
    <div class="smalltext">Seconds</div> 
</div> 
</div> 

<p id="demo"></p> 
                <img src="{{ asset('assets/images/icon_comment.png') }}" class="img-fluid icon_chat">
                <h3>
                    Say Hi to<br>
                    Connecting Easily
                </h3>
                <h6>It’s free, Download it now</h6>
                <ul class="nav justify-content-center ">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('assets/images/download/google_play_banner.svg') }}">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('assets/images/download/app_store_banner.svg') }}">
                        </a>
                    </li>
                </ul>
                <ul class="nav justify-content-center download_client">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('assets/images/download/icons8-windows_client.svg') }}">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('assets/images/download/icons8-linux_client.svg') }}">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('assets/images/download/icons8-mac_client.svg') }}">
                        </a>
                    </li>
                </ul>-->
            </div>
        </div>
    </div>
</div>
<section class="section" id="home_why">
    <div class="container">
        <div class="row">
            <div class="col-12 section_title ">
                <h3>Why HiApp is for you?</h3>
                <h6>Strengths on Point</h6>
            </div>
        </div>
        <div class="row">
            @forelse($data_why as $why)
                <div class="col-sm-6 col-md-4 ">
                    <div class=" card_item_why">
                        <img src="{{ $why->url_images() }}" class="img-fluid">
                        <h3>{{ App::getLocale() == 'en' ? $why->title_en : $why->title_ind }}</h3>
                        <p>
                            {!! App::getLocale() == 'en' ? $why->content_en : $why->content_ind !!}
                        </p>
                    </div> 
                </div>
            @empty 
            @endforelse
            

        </div>
    </div>
</section>

<section class="section" id="home_do">
    <div class="container">
        <div class="row">
            <div class="col-12 section_title ">
                <h3>What can HiApp do for you?</h3>
                <h6>Solutions for Everyone</h6>
            </div>
        </div>
        <div class="row">
            @forelse($data_can as $can)
                <div class="col-12 col-sm-6 col-md-3 ">
                    <div class=" card_item_do">
                        @if($can->coming_soon == '1')
                            <img src="{{ asset('assets/images/coming_label.svg') }}" class="coming">
                        @endif
                        <div class="text-center">
                            <img src="{{ $can->url_images() }}" class="img-fluid">
                        </div>
                        <div class="content">
                            <h3>{{ App::getLocale() == 'en' ? $can->title_en : $can->title_ind }}</h3>
                            <p>
                                {!! App::getLocale() == 'en' ? $can->content_en : $can->content_ind !!}
                            </p>
                            <a href="" class="more">Learn More <i class="fa fa-chevron-right ml-3"></i></a>
                        </div>
                    </div> 
                </div>
            @empty 
            @endforelse
            
            

        </div>
    </div>
</section>
<div id="banner_download_home" class="jumbotron jumbotron-fluid" >
    <div class="container">
        <div class="row d-flex align-items-center">
            
            <div class="col-md-6 content_left">
                <div class="download_box">
                    <img src="{{ asset('assets/images/download/download_box.svg') }}" class="img-fluid ">
                </div>
                <div class="content_download">
                    Try HiApp for Free, invite your contacts in a flash and make it easy to stay connected! <br>* Data charges may apply. Contact your provider for details.
                </div>
                <ul class="nav justify-content-start list_download">
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('assets/images/download/google_play_dark.svg') }}">
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="{{ asset('assets/images/download/app_store_dark.svg') }}">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="d-none d-sm-block col-md-6 text-center">
                <img src="{{ asset('assets/images/download/download_right.svg') }}" class="img-fluid home_download_hp">
            </div>
        </div>
    </div>
</div>-->
@endsection
