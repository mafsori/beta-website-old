<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="{{ $website->caption }} ">
        <meta name="keywords" content="{{ $website->name }}">
        <title>{{ $website->name }} - {{ $website->caption }}</title>
        <!-- Fonts -->
        {!! SEO::generate() !!}
        
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('assets_admin/css/core.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets_admin/css/app.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets_admin/css/style.min.css') }}" rel="stylesheet">
        <!-- Favicons -->
    </head>
    <body>
        <div class="row no-gutters min-h-fullscreen bg-white">
            <div class="col-md-6 col-lg-7 col-xl-8 d-none d-md-block bg-img" style="background-image: url({{ asset('assets/images/bg_login.png') }})" data-overlay="5">
                <div class="row h-100 pl-50">
                    <div class="col-md-10 col-lg-8 align-self-end">
                        <img src="{{ $website->url_logo() }}" alt="...">
                        <br><br><br>
                        <h4 class="text-white">{{ $website->name }}</h4>
                        <p class="text-white">{{ $website->caption }}</p>
                        <br><br>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-5 col-xl-4 align-self-center">
                <div class="px-80 py-30">
                    <h4>Login</h4>
                    <p><small>Sign into your account</small></p>
                    <br>
                    <form class="form-type-material" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
                            <label for="email">Email</label>
                            @if ($errors->has('email'))
                                <div class="text-danger">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" id="password">
                            <label for="password">Password</label>
                            @if ($errors->has('password'))
                                <div class="text-danger">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                        <div class="form-group flexbox">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="remember" checked>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Remember me</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-bold btn-block btn-primary" type="submit">Login</button>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        <!-- Scripts -->
        <script src="{{ asset('assets_admin/js/core.min.js') }}"></script>
        <script src="{{ asset('assets_admin/js/app.min.js') }}"></script>
        <script src="{{ asset('assets_admin/js/script.min.js') }}"></script>
    </body>
</html>