@extends('layouts.admin')

@section('content')
<form action="{{ url('admin/profile') }}" method="POST" role="form" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title"><strong>Detail Data</strong></h5>
                </div>
                <div class="card-body">
                    
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required>
                    </div>
                   
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" class="form-control" readonly value="{{ Auth::user()->email }}" required>
                    </div>
                    
                    
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="text" class="form-control" name="password">
                        <div class="text-danger">Kosongi jika tidak merubah password</div>
                    </div>    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title"><strong>Opsi Data</strong></h5>
                </div>
                <div class="card-body">
                    
                    <div class="form-group">
                        <label for="">Foto</label>
                        <div class="input-group file-group">
                            <input type="text" class="form-control file-value" placeholder="Choose file..." readonly="">
                            <input type="file" name="avatar" onchange="LoadImage(this)">
                            <span class="input-group-btn">
                            <button class="btn btn-light file-browser" type="button"><i class="fa fa-upload"></i></button>
                            </span>
                        </div>
                        <img src="{{ Auth::user()->url_images() }}" id="pic" style="max-width: 100%;">
                    </div>
                    

                    <button type="submit" class="btn btn-primary">Simpan</button>
                    
                </div>
            </div>
        </div>
    </div>
        
</form>
@endsection
            