@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card card-default ">
                <div class="card-header">
                    <h4 class="card-title mb-0">Data User Admin</h4>
                </div>
                <div class="card-body card-table">
                    <div class="table-responsive">
                        <table class="table table-hover datatable">
                            <thead>
                                <tr>
                                    <th>Opsi</th>
                                    <th>Avatar</th>
                                    <th>Nama</th>
                                    <th>Email</th> 
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($user_admin as $admin)
                                    <tr>
                                        <td>
                                            <a href="#modal_edit" class="text-warning p-r-5" 
                                                data-toggle="modal"  
                                                data-toggle="tooltip" 
                                                
                                                title="edit" 
                                                data-original-title="Edit"
                                                data-id="{{ $admin->id }}" 
                                                data-avatar="{{ $admin->url_images() }}" 
                                                data-name="{{ $admin->name }}" 
                                                data-email="{{ $admin->email }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="{{ url("admin/users/destroy", $admin->id) }}" class="text-danger" 
                                                title="delete" 
                                                data-toggle="tooltip" 
                                                data-original-title="Delete"
                                                onclick="return confirm('Are You Sure Delete?')"
                                                >
                                                <i class="la la-trash"></i>
                                            </a>
                                        
                                        </td>
                                        <td><img src="{{ $admin->url_images() }}" width="50px"></td>
                                        <td>{{ $admin->name }}</td>
                                        <td>{{ $admin->email }}</td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                            {{-- <tfoot>
                                <tr>
                                    <td colspan="4">{{ $user_admin->links() }}</td>
                                </tr>
                            </tfoot> --}}
                        </table>
                    </div>
                        
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-default">
                <div class="card-header">
                    <h4 class="card-title mb-0">Add User Admin</h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('admin/users') }}" method="POST" role="form" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">name</label>
                            <input type="text" class="form-control"  name="name">
                        </div>
                        <div class="form-group">
                            <label for="">email</label>
                            <input type="email" class="form-control"  name="email">
                        </div>
                        <div class="form-group">
                            <label for="">password</label>
                            <input type="password" class="form-control"  name="password">
                        </div>
                        <div class="form-group">
                            <div class="input-group file-group">
                                <input type="text" class="form-control file-value" placeholder="Choose file..." readonly="">
                                <input type="file" name="avatar" onchange="LoadImage(this)">
                                <span class="input-group-btn">
                                    <button class="btn btn-light file-browser" type="button"><i class="fa fa-upload"></i></button>
                                </span>
                                
                            </div>
                            <img src="" id="pic" style="max-width: 100%;">
                        </div>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Admin</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    
                </div>
                <form action="{{ url('admin/users/update') }}" method="POST" role="form" enctype="multipart/form-data">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group file-group">
                                <input type="text" class="form-control file-value" placeholder="Choose file..." readonly="">
                                <input type="file" name="avatar" onchange="LoadImage2(this)">
                                <span class="input-group-btn">
                                    <button class="btn btn-light file-browser" type="button"><i class="fa fa-upload"></i></button>
                                </span>
                                
                            </div>
                            <img src="" id="pic2" style="max-width: 100%;">
                        </div>
                        <div class="form-group">
                            <label for="">name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="">email</label>
                            <input type="email" class="form-control" id="email" name="email" disabled>
                        </div>
                        <div class="form-group">
                            <label for="">password</label>
                            <input type="password" class="form-control"  name="password">
                            <span class="help-block text-danger">Kosongi jika tidak ganti password</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
                    
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $('#modal_edit').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') 
            var name = button.data('name') 
            var email = button.data('email') 
            var avatar = button.data('avatar') 
            var modal = $(this)
            modal.find('#id').val(id)
            modal.find('#name').val(name)
            modal.find('#email').val(email)
            modal.find('#pic2').attr('src', avatar)
        })
    </script>
@endsection