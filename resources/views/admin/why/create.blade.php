@extends('layouts.admin')

@section('content')
    
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <h3 class="page-title">Why HiApp</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{ url('/admin/why') }}">Why HiApp</a></li>
                    <li class="breadcrumb-item active">add</li>
                </ul>
            </div>
        </div>
    </div>
    <form action="{{ url('admin/why') }}" method="POST" role="form" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Detail </h4>
                    </div>
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#ind">Indonesia</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#en">English</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="ind">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="">Title</label>
                                        <input type="text" class="form-control" name="title_ind" value="{{ old('title_ind') }}" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="">content</label>
                                        <textarea class="form-control" name="content_ind" required>{{ old('content_ind') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="en">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="">Title</label>
                                        <input type="text" class="form-control" name="title_en" value="{{ old('title_en') }}" required>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="">content</label>
                                        <textarea class="form-control" name="content_en" required>{{ old('content_en') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-0"> why</h4>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="">urutan</label>
                            <input type="number" class="form-control" name="urutan" value="{{ old('urutan') }}" required>
                        </div>
                        <div class="form-group">
                            <label for="image" class=" control-label">Image</label>
                            <div class="input-group file-group">
                                <input type="text" class="form-control file-value" placeholder="Choose file..." readonly="">
                                <input type="file" name="images" onchange="LoadImage(this)" required accept="image/*">
                                <span class="input-group-btn">
                                    <button class="btn btn-light file-browser" type="button"><i class="fa fa-upload"></i></button>
                                </span>
                            </div>
                            <img src="" id="pic" style="max-width: 100%;">
                        </div>
                        <div class="form-group">
                            <label for="">Status</label>
                            {{ Form::select('status', \App\Model\HelperData::list_status(), null,['class'=>'form-control select2', 'required'=>true])}}
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                        
                    </div>
                </div>
            </div>
        </div>
           
    </form>
@endsection