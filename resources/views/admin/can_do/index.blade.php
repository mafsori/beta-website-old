@extends('layouts.admin')

@section('content')

    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <h3 class="page-title">Can DO</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Can DO</li>
                </ul>
            </div>
            <div class="col-auto float-right ml-auto text-capitalize">
                <a href="{{ url('admin/can_do/create') }}" class="btn add-btn" ><i class="fa fa-plus"></i> Add</a>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover datatable">
                    <thead>
                        <tr>
                            <th>Opsi</th>
                            <th>Images</th>
                            <th>Title en</th>
                            <th>Title ind</th>
                            <th>urutan</th>
                            <th>status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($data_all as $data)
                            <tr>
                                <td>
                                    <a href="{{url('admin/can_do/'.$data->id.'/edit')}}" class="text-warning"><i class="fa fa-edit"></i></a>
                                        <a href="{{url('admin/can_do/destroy/'.$data->id)}}" class="text-danger" onclick="return confirm('Are You Sure Delete?')"><i class="fa fa-trash"></i></a>
                                </td>
                                <td><img src="{{ $data->url_images() }}" width="50px"></td>
                                <td>{{ $data->title_en }}</td>
                                <td>{{ $data->title_ind }}</td>
                                <td>{{ $data->urutan }}</td>
                                <td>{{ $data->status_text() }}</td>
                            </tr>
                        @empty 
                            <tr>
                                <td>
                                    Data Kosong
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>


@endsection
