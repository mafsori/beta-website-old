@extends('layouts.admin')

@section('content')
    
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <h3 class="page-title">website</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">website</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <form action="{{ url('admin/website') }}" method="POST" role="form" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <h5 class=""><strong>Detail Data</strong></h5>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-tabs nav-tabs-inverse-mode">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab_profile">Profile</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#sosmed_tab">Sosmed</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#seo_tab">Seo</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="tab_profile">
                                        <div class="form-group">
                                            <label for="">Name website</label>
                                            <input type="text" class="form-control" name="name" value="{{ $website->name }}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Caption</label>
                                            <input type="text" class="form-control" name="caption" value="{{ $website->caption }}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Url</label>
                                            <input type="text" class="form-control" name="url" value="{{ $website->url }}" required>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane fade" id="sosmed_tab">
                                        <div class="form-group">
                                            <label for="">facebook</label>
                                            <input type="text" class="form-control" name="facebook" value="{{ $website->facebook }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">twitter</label>
                                            <input type="text" class="form-control" name="twitter" value="{{ $website->twitter }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">instagram</label>
                                            <input type="text" class="form-control" name="instagram" value="{{ $website->instagram }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">linkedin</label>
                                            <input type="text" class="form-control" name="linkedin" value="{{ $website->linkedin }}">
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="seo_tab">
                                        <div class="form-group">
                                            <label for="">seo title</label>
                                            <input type="text" class="form-control" name="seo_title" value="{{ $website->seo_title }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">seo desc</label>
                                            <input type="text" class="form-control" name="seo_desc" value="{{ $website->seo_desc }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">seo keyword</label>
                                            <input type="text" class="form-control" name="seo_keyword" value="{{ $website->seo_keyword }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">seo tag</label>
                                            <input type="text" class="form-control" name="seo_tag" value="{{ $website->seo_tag }}">
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header">
                                <h5 class=""><strong>Opsi Data</strong></h5>
                            </div>
                            <div class="card-body ">
                                
                                <div class="form-group">
                                    <label for="">logo</label>
                                    <div class="input-group file-group">
                                        <input type="text" class="form-control file-value" placeholder="Choose file..." readonly="">
                                        <input type="file" name="logo" onchange="LoadImage(this)">
                                        <span class="input-group-btn">
                                        <button class="btn btn-light file-browser" type="button"><i class="fa fa-upload"></i></button>
                                        </span>
                                    </div>
                                    <img src="{{ $website->url_logo() }}" id="pic" style="max-width: 100%;">
                                </div>
                                <div class="form-group">
                                    <label for="">icon</label>
                                    <div class="input-group file-group">
                                        <input type="text" class="form-control file-value" placeholder="Choose file..." readonly="">
                                        <input type="file" name="icon" onchange="LoadIcon(this)">
                                        <span class="input-group-btn">
                                        <button class="btn btn-light file-browser" type="button"><i class="fa fa-upload"></i></button>
                                        </span>
                                    </div>
                                    <img src="{{ $website->url_icon() }}" id="picIcon" style="max-width: 100%;">
                                </div>
                                

                                <button type="submit" class="btn btn-primary btn-block">Update</button>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection