@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-6 col-xl-3">
        <div class="card card-body bg-primary">
            <div class="flexbox">
                <span class="ti-thumb-up fs-40"></span>
                <span class="fs-40 fw-100">{{ App\Model\Why::get()->count() }}</span>
            </div>
            <div class="text-right">Why HiApp</div>
        </div>
    </div>
    <div class="col-md-6 col-xl-3">
        <div class="card card-body bg-purple">
            <div class="flexbox">
                <span class="ti-check-box fs-40"></span>
                
                <span class="fs-40 fw-100">{{ App\Model\CanDo::get()->count() }}</span>
            </div>
            <div class="text-right">Can Do HiApp</div>
        </div>
    </div>
</div>
@endsection
