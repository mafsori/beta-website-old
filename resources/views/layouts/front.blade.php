<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.png') }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav id="navbar_top" class="navbar navbar-expand-md ">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('assets/images/main/logo_top.svg') }}" class="img-fluid">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_navbar" aria-controls="main_navbar" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="fa fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="main_navbar">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <i class="fa fa-world"></i>@lang('menu.About') <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/about/company') }}">
                                    @lang('menu.Company')
                                </a>
                                <a class="dropdown-item" href="{{ url('/about/csr') }}">
                                    @lang('menu.CSR')
                                </a>
                                <!--<a class="dropdown-item" href="{{ url('/careers') }}">
                                    @lang('menu.Careers')
                                </a>
                                <a class="dropdown-item" href="{{ url('/community') }}">
                                    @lang('menu.Community')
                                </a>-->
                                <a class="dropdown-item" href="{{ url('/contact_us') }}">
                                    @lang('menu.Contact Us')
                                </a>
                            </div>
                        </li>
                         <!--<li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <i class="fa fa-world"></i>@lang('menu.Product') <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu " aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/product/hiapp') }}">
                                    @lang('menu.Hi Chat')
                                </a>
                                <a class="dropdown-item" href="{{ url('/product/hiconference') }}">
                                    @lang('menu.Hi Conference')
                                </a>
                                <a class="dropdown-item" href="{{ url('/product/hibusiness') }}">
                                    @lang('menu.Hi Business')
                                </a>
                                <a class="dropdown-item" href="{{ url('/product/hipay') }}">
                                    @lang('menu.Hi Pay')
                                </a>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/security') }}">@lang('menu.Security')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/faq') }}">@lang('menu.FAQ')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/download') }}">@lang('menu.Download')</a>
                        </li>
                    </ul>

                     Right Side Of Navbar 
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <img src="{{ asset('assets/images/icons8-globe_earth.svg') }}" class="mr-2">
                                    @if(App::getLocale() == 'en')
                                        English
                                    @else
                                        Indonesia
                                    @endif 
                                <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/change_language/ind') }}">
                                    Indonesia
                                </a>
                                <a class="dropdown-item" href="{{ url('/change_language/en') }}">
                                    English
                                </a>
                            </div>
                        </li>
                    </ul>-->
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
            
        </main>
        <footer>
            <!--<img id="logo_footer" src="{{ asset('assets/images/main/logo_bottom.svg') }}">-->
            <div id="footer_top">
                
                
                <!--<div class="container">
                    <div class="row">
                        <div class="col-md-3 ">
                            
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-3 menu_footer">
                                    <h4>@lang('menu.About')</h4>
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/about/company') }}">@lang('menu.Company')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/about/csr') }}">@lang('menu.CSR')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/careers') }}">@lang('menu.Careers')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/features') }}">@lang('menu.Features')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/security') }}">@lang('menu.Security')</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 menu_footer">
                                    <h4>@lang('menu.Product')</h4>
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/product/hiapp') }}">@lang('menu.Hi Chat')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/product/hiconference') }}">@lang('menu.Hi Conference')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/product/hibusiness') }}">@lang('menu.Hi Business')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/product/hipay') }}">@lang('menu.Hi Pay')</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 menu_footer">
                                    <h4>@lang('menu.Download')</h4>
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/') }}">Desktop</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/') }}">Android Mobile</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/') }}">Iphone iOs</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/') }}">Windows Phone</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3 menu_footer">
                                    <h4>@lang('menu.Help Center')</h4>
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/faq') }}">@lang('menu.FAQ')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/contact_us') }}">@lang('menu.Contact Us')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/community') }}">@lang('menu.Community')</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ url('/privacy') }}">@lang('menu.Privacy and Terms')</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
            <div id="footer_bottom">
                <div class="container">
                    <div class="row d-flex align-items-center">
                        <div class="col-md-3">
                            <i class="fa fa-copyright"></i> 2020 - HiApp. All Right Reserved
                        </div>
                        <div class="col-md-6 text-center">
                            HiApp is a trademark of PT. Hello Kreasi Indonesia.Registered in the Directorate General of Intellectual Property of the Republic of Indonesia
                        </div>
                        <div class="col-md-3">
                            <ul class="nav justify-content-center footer_sosmed">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ $website->facebook }}" target="_blank">
                                        <img src="{{ asset('assets/images/sosmed/icon_fb.svg') }}" class="img-fluid">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ $website->twitter }}" target="_blank">
                                        <img src="{{ asset('assets/images/sosmed/icon_twitter.svg') }}" class="img-fluid">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ $website->instagram }}" target="_blank">
                                        <img src="{{ asset('assets/images/sosmed/icon_instagram.svg') }}" class="img-fluid">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ $website->linkedin }}" target="_blank">
                                        <img src="{{ asset('assets/images/sosmed/icon_linkedin.svg') }}" class="img-fluid">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>    
                </div>
            </div>
                
        </footer>
    </div>
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/slick/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.slider').slick({
                autoplay: true,
                autoplaySpeed: 2000,
                dots:true,
                arrows:false
            });
        });
    </script>
    @yield('js')
</body>
</html>
