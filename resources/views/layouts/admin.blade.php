<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content=" ">
        <meta name="keywords" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="icon" type="image/png" sizes="16x16" href="{{ $website->url_icon() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Fonts -->
        <!-- Styles -->
        <link href="{{ asset('assets_admin/css/core.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets_admin/css/app.min.css') }}" rel="stylesheet">
        
    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader">
            <div class="spinner-dots">
                <span class="dot1"></span>
                <span class="dot2"></span>
                <span class="dot3"></span>
            </div>
        </div>
        <!-- Sidebar -->
        <aside class="sidebar sidebar-icons-right sidebar-icons-boxed sidebar-expand-lg">
            <header class="sidebar-header" style="background-color: #dad7de">
                
                <span class="logo">
                    <a href="{{ url('/home') }}"><img src="{{ $website->url_logo() }}" alt="logo" style="max-height: 50px"></a>
                </span>
                <span class="sidebar-toggle-fold"></span>
            </header>
            <nav class="sidebar-navigation">
                <ul class="menu">
                    <li class="menu-category">Main Navigation</li>
                    <li class="menu-item {{ (\Request::is('/home')) || (\Request::is('/home/*')) ? 'active' : '' }}">
                        <a class="menu-link" href="{{ url('/home') }}">
                            <span class="icon fa fa-home"></span>
                            <span class="title">Home</span>
                        </a>
                    </li>

                    <li class="menu-item {{ (\Request::is('admin/why')) || (\Request::is('admin/why/*')) ? 'active' : '' }}">
                        <a class="menu-link" href="{{ url('admin/why') }}">
                            <span class="icon ti-thumb-up"></span>
                            <span class="title"> Why HiApp</span>
                        </a>
                    </li>
                    <li class="menu-item {{ (\Request::is('admin/can_do')) || (\Request::is('admin/can_do/*')) ? 'active' : '' }}">
                        <a class="menu-link" href="{{ url('admin/can_do') }}">
                            <span class="icon ti-check-box"></span>
                            <span class="title"> can do HiApp</span>
                        </a>
                    </li>
                    
                    <li class="menu-category">Settings</li>
                    <li class="menu-item  {{  (\Request::is('admin/users')) || (\Request::is('admin/users/admin/*'))  || (\Request::is('admin/website')) || (\Request::is('admin/website/*')) ? 'active open' : '' }}">
                        <a class="menu-link" href="#">
                            <span class="icon fa fa-gears"></span>
                            <span class="title">Settings</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="menu-submenu">
                            <li class="menu-item {{ (\Request::is('admin/users')) || (\Request::is('admin/users/*')) ? 'active' : '' }}">
                                <a class="menu-link" href="{{ url('admin/users') }}">
                                    <span class="dot"></span>
                                    <span class="title">Admin</span>
                                </a>
                            </li>
                            <li class="menu-item {{ (\Request::is('admin/website')) || (\Request::is('admin/website/*')) ? 'active' : '' }}">
                                <a class="menu-link" href="{{ url('admin/website') }}">
                                    <span class="dot"></span>
                                    <span class="title">Website</span>
                                </a>
                            </li>
                            
                        </ul>
                    </li>
                    
                </ul>
            </nav>
        </aside>
        <!-- END Sidebar -->
        <!-- Topbar -->
        <header class="topbar">
            <div class="topbar-left">
                <span class="topbar-btn sidebar-toggler"><i>&#9776;</i></span>
<!--                <a class="topbar-btn d-none d-md-block" href="#" data-provide="fullscreen tooltip" title="Fullscreen">
                    <i class="material-icons fullscreen-default">fullscreen</i>
                    <i class="material-icons fullscreen-active">fullscreen_exit</i>
                </a>-->
            </div>
            <div class="topbar-right">
                <ul class="topbar-btns">
                    <li class="dropdown">
                        <span class="topbar-btn" data-toggle="dropdown">{{ Auth::user()->name }} <img class="avatar" src="{{ Auth::user()->url_images() }}" alt="..."> </span>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ url('/admin/profile') }}"><i class="ti-user"></i> Profile</a>
                            
                            <a class="dropdown-item" href="{{ url('/admin/profile') }}"><i class="ti-settings"></i> Settings</a>
                            <div class="dropdown-divider"></div>
                            

                            <a class="dropdown-item" href="{{ url('/logout') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="ti-power-off"></i> Logout
                            </a>

                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </header>
        <!-- END Topbar -->
        <!-- Main container -->
        <main>
            <div class="main-content">
                @if(Session::has('flash_message')) 
                <div class="alert alert-info">
                    {{ Session::get('flash_message')}}
                </div>
                @endif
                
                @yield('content')
            </div><!--/.main-content -->
            <!-- Footer -->
            <footer class="site-footer">
                <div class="row">
                    <div class="col-md-6">
                        <p class="text-center text-md-left">Copyright © 2019. All rights reserved.</p>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </main>
        <!-- END Main container -->
        
        <!-- Scripts -->
        <script src="{{ asset('assets_admin/js/core.min.js') }}" data-provide="sweetalert"></script>
        <script src="{{ asset('assets_admin/js/app.min.js') }}"></script>
        <script src="{{ asset('assets_admin/js/script.min.js') }}"></script>
        <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
        <script>
            app.ready(function(){
                @if(Session::has('success')) 
                    swal({
                        title: 'Berhasil',
                        text: '{{ Session::get('success') }}',
                        type: 'success',
                        timer: 2000
                    });
                @endif

                @if(Session::has('warning')) 
                    swal({
                        title: 'Gagal',
                        text: '{{ Session::get('warning') }}',
                        type: 'warning',
                        timer: 2000
                    });
                @endif

                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        
                        app.toast('', {
                            actionTitle: '{{ $error }}',
                            actionUrl: '#',
                            actionColor: 'danger'
                        });
                    @endforeach
                @endif
                $('.textarea').ckeditor({
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=' + $("input[name=_token]").val(),
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token=' + $("input[name=_token]").val()
                });
                
            });
            function LoadImage(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#pic').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }
            function LoadImage2(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#pic2').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }  
            function LoadImageMultiple(input) {
                $("#filePreview").html('');
                if (input.files) {
                    var files = input.files;
                    var filesLength = files.length ;
                    for (var i = 0; i < filesLength ; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            var dom = '<img src="'+e.target.result+'" class="img img-thumbnail" width="130" height="100" style="margin:5px;">';
                            $("#filePreview").append(dom);
                       });
                       fileReader.readAsDataURL(f);
                   }
                }
            }
        </script>
        @yield('js')
    </body>
</html>