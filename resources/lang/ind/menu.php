<?php

return [

    'About' => 'Tentang',
    'Company' => 'Company',
    'CSR' => 'CSR',
    'Careers' => 'Careers',
    'Community' => 'Community',
    'Contact us' => 'Contact us',
    'Product' => 'Product',
    'Hi Chat' => 'Hi Chat',
    'Hi Conference' => 'Hi Conference',
    'Hi Business' => 'Hi Business',
    'Hi Pay' => 'Hi Pay',
    'Security' => 'Security',
    'FAQ' => 'FAQ',
    'Download' => 'Download',
    'Features' => 'Features',
    'Help Center' => 'Help Center',
    'Privacy and Terms' => 'Privacy and Terms',

];
