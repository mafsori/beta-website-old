<?php

use Illuminate\Database\Seeder;

class website_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('websites')->insert([
            'url' => 'http://hiapp.id/',
            'name' => 'hiapp',
            'caption' => 'Fast And Secure Messenger',
            'icon' => 'icon.png',
            'logo' => 'logo.png',
            'facebook' => 'https://www.facebook.com/',
            'twitter' => 'https://www.twitter.com/',
            'instagram' => 'https://www.instagram.com/',
            'linkedin' => 'https://www.linkedin.com/',
            'seo_keyword' => 'hiapp Fast And Secure Messenger',
            'seo_tag' => 'hiapp Fast And Secure Messenger',
            'seo_title' => 'hiapp Fast And Secure Messenger ',
            'seo_desc' => 'hiapp Fast And Secure Messenger '
        ]);
    }
}
