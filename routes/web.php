<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/change_language/{locale}', 'PageController@change_language');

Route::get('/', 'PageController@index');
Route::get('/comingsoon', 'PageController@comingsoon');
Route::get('/about/company', 'PageController@about_company');
Route::get('/about/csr', 'PageController@about_csr');

Route::get('/product/hiapp', 'PageController@product_hiapp');
Route::get('/product/hiconference', 'PageController@product_hiconference');
Route::get('/product/hibusiness', 'PageController@product_hibusiness');
Route::get('/product/hipay', 'PageController@product_hipay');

Route::get('/careers', 'PageController@careers');
Route::get('/features', 'PageController@features');
Route::get('/security', 'PageController@security');
Route::get('/faq', 'PageController@faq');
Route::get('/contact_us', 'PageController@contact_us');
Route::get('/community', 'PageController@community');
Route::get('/privacy', 'PageController@privacy');
Route::get('/download', 'PageController@download');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();


Route::group(['prefix' => 'admin','namespace' => 'Admin','middleware' => ['web', 'auth']], function () {

	Route::get('website', 'WebsiteController@index');
	Route::post('website', 'WebsiteController@update');

	Route::get('/profile', 'UsersController@profile');
	Route::post('/profile', 'UsersController@profile_update');

	Route::resource('/users', 'UsersController', ['except' => ['destroy']]);
	Route::get('/users/destroy/{id}', 'UsersController@destroy');

	Route::resource('/why', 'WhyController', ['except' => ['destroy']]);
	Route::get('/why/destroy/{id}', 'WhyController@destroy');

	Route::resource('/can_do', 'CanDoController', ['except' => ['destroy']]);
	Route::get('/can_do/destroy/{id}', 'CanDoController@destroy');

});